import computation.contextfreegrammar.ContextFreeGrammar;
import computation.contextfreegrammar.Rule;
import computation.contextfreegrammar.Symbol;
import computation.contextfreegrammar.Word;
import computation.derivation.Derivation;
import computation.derivation.Step;
import computation.parser.IParser;
import computation.parsetree.ParseTreeNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * ParserFirstAlgorithm implements the first algorithm
 */
public class ParserFirstAlgorithm implements IParser {

    /**
     * This method provides an implementation of IParser::generateParseTree interface.
     * @param cfg the context free grammar
     * @param w the word to test
     * @return - a valid ParseTreeNode for the String or null
     */
    @Override
    public ParseTreeNode generateParseTree(ContextFreeGrammar cfg, Word w) {

        Derivation derivation = findDerivationForWord(cfg, w);

        if(derivation == null){
            return null;
        }

        return getParseTree(derivation);
    }

    /**
     * This method provides an implementation of IParser::isInLanguage interface.
     * @param cfg the context free grammar
     * @param w the word to test
     * @return - a boolean value indicating if the String is in the language
     */
    @Override
    public boolean isInLanguage(ContextFreeGrammar cfg, Word w) {
        Derivation derivation = findDerivationForWord(cfg, w);
        return derivation != null;
    }

    /**
     * This method handles the private implementation that is common to both
     * isInLanguage() and generateParseTree(). It returns a Derivation for the String or null.
     * Throws an IllegalArgumentException if the grammar parameter is not in CNF.
     * @param cfg - a context free grammar in CNF
     * @param w - String being tested
     * @return - A derivation for the String being tested or null.
     */
    private Derivation findDerivationForWord(ContextFreeGrammar cfg, Word w) {

        if(!cfg.isInChomskyNormalForm()){
            throw new IllegalArgumentException("The grammar provided is not in CNF.");
        }

        List<Derivation> possibleDerivation = getAllPossibleDerivations(cfg, calculateNumberOfSteps(w));
        Iterator<Derivation> iterator = possibleDerivation.iterator();

        while(iterator.hasNext()){
            Derivation nextDerivation = iterator.next();
            Word latestWord = nextDerivation.getLatestWord();
            if(latestWord.equals(w)){
                return nextDerivation;
            }
        }

        return null;
    }

    /**
     * Converts a Derivation into a ParseTreeNode
     * @param derivation - Derivation to convert
     * @return - ParseTreeNode for the Derivation
     */
    private ParseTreeNode getParseTree(Derivation derivation){

        Iterator<Step> iterator = derivation.iterator();
        Stack<ParseTreeNode> childNodes = new Stack<>();

        while(iterator.hasNext()){
            Step next = iterator.next();
            Rule rule = next.getRule();

            if(rule != null && rule.getExpansion().isTerminal()){
                childNodes.push(getTerminalParseTreeNode(rule));
            }else if(childNodes.size() >= 2){
                childNodes.push(new ParseTreeNode(rule.getVariable(), childNodes.pop(), childNodes.pop()));
            }
        }

        return childNodes.pop();
    }

    /**
     * Private method that converts a given rule to a ParseTreeNode
     * @param rule - rule to convert to node
     * @return - ParseTreeNode from the rule
     */
    private ParseTreeNode getTerminalParseTreeNode(Rule rule) {
        return new ParseTreeNode(rule.getVariable(),
                new ParseTreeNode(rule.getExpansion().get(0)));
    }

    /**
     * This method computes all possible Derivations in n steps
     * @param cfg - context free grammar
     * @param numberOfSteps - number of steps for breath search
     * @return - list of derivations in n steps
     */
    private List<Derivation> getAllPossibleDerivations(ContextFreeGrammar cfg, int numberOfSteps) {

        Derivation stepMinusOne = new Derivation(new Word(cfg.getStartVariable()));

        List<Derivation> possibleDerivation = new ArrayList<>();
        possibleDerivation.add(stepMinusOne);

        for (int i = 0; i < numberOfSteps; i++) {
            possibleDerivation = getAllPossibleDerivationsInOneStep(i, cfg, possibleDerivation);
        }
        return possibleDerivation;
    }

    /**
     * Determines the list of possible Derivations in one step
      * @param index - index of the Derivation for this step
     * @param cfg - context free grammar
     * @param input - List of Derivations as input
     * @return - list of possible derivations in one step
     */
    private List<Derivation> getAllPossibleDerivationsInOneStep(int index,
                                                                ContextFreeGrammar cfg,
                                                                List<Derivation> input) {
        List<Derivation> possibleDerivationInOneStep = new ArrayList<>();
        List<Rule> rules = cfg.getRules();

        for (Derivation d :  input) {
            Word latestWord = d.getLatestWord();

            for (int i = 0; i < latestWord.length(); i++) {
                Symbol symbol = latestWord.get(i);
                for (Rule rule: rules) {
                    if(symbol.equals(rule.getVariable())){
                        Derivation newDerivation = new Derivation(d);
                        newDerivation.addStep(latestWord.replace(i, rule.getExpansion()), rule, index);
                        possibleDerivationInOneStep.add(newDerivation);
                    }
                }
            }
        }

        return possibleDerivationInOneStep;
    }

    /**
     * Calculates the number of steps required for a given word
     * @param w - String to calculate the steps for.
     * @return - the number of steps calculated.
     */
    private int calculateNumberOfSteps(Word w) {
        int n = w.length();

        if(n == 0){
            return 1;
        }

        return 2 * n - 1;
    }

}
