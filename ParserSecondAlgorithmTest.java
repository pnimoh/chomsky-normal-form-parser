import computation.contextfreegrammar.*;
import computation.parser.IParser;
import computation.parsetree.ParseTreeNode;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ParserSecondAlgorithmTest {

    @Test(expected = IllegalArgumentException.class)
    public void isInLanguage_grammarNotInCNF_expectException(){
        ContextFreeGrammar contextFreeGrammar = new ContextFreeGrammar(List.of(new Rule(new Variable('X'), new Word("XYZ"))));
        Word wordUnderTest = new Word("x+x");
        IParser parser = new ParserSecondAlgorithm();
        parser.isInLanguage(contextFreeGrammar, wordUnderTest);


    }

    @Test
    public void isInLanguage_emptyStringUnderTest_grammarHasRule(){
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar cfg = MyGrammar.makeGrammarThatAcceptsEmptyString();

        boolean inLanguage = parser.isInLanguage(cfg, Word.emptyWord);
        assertTrue(inLanguage);
    }

    @Test
    public void isInLanguage_xTimesxUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parser.isInLanguage(contextFreeGrammar, new Word("x*x"));

        assertTrue(inLanguage);
    }

    @Test
    public void isInLanguage_xTimesXPlusXUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parser.isInLanguage(contextFreeGrammar, new Word("x*x+x"));

        assertTrue(inLanguage);
    }

    @Test
    public void isInLanguage_firstExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parser.isInLanguage(contextFreeGrammar, new Word("(((x+(x)*x)*x)*x)"));

        assertTrue(inLanguage);
    }

    @Test
    public void isInLanguage_secondExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parser.isInLanguage(contextFreeGrammar, new Word("x*x+x+x*(x)"));

        assertTrue(inLanguage);
    }

    @Test
    public void isInLanguage_thirdExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parser.isInLanguage(contextFreeGrammar, new Word("(x*x)*x+x+(x)"));

        assertTrue(inLanguage);
    }

    @Test
    public void isInLanguage_fourthExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        //(x+x))∗((x)∗x
        boolean inLanguage = parser.isInLanguage(contextFreeGrammar, new Word("(x+x))*((x)*x"));

        assertFalse(inLanguage);
    }

    @Test
    public void generateParseTree_emptyWordUnderTest_expectParseTree(){
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar cfg = MyGrammar.makeGrammarThatAcceptsEmptyString();

        ParseTreeNode parseTreeNode = parser.generateParseTree(cfg, Word.emptyWord);

        ParseTreeNode expectedParseTreeNode = ParseTreeNode.emptyParseTree(new Variable('S'));
        assertEquals(expectedParseTreeNode, parseTreeNode);
    }

    @Test
    public void generateParseTree_xPlusxUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode node = parser.generateParseTree(contextFreeGrammar, new Word("x+x"));

        node.print();

        assertNotNull(node);

    }

    @Test
    public void generateParseTree_xTimesXPlusXUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode node = parser.generateParseTree(contextFreeGrammar, new Word("x*x+x"));
        node.print();
        assertNotNull(node);
    }

    @Test
    public void generateParseTree_firstExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode node = parser.generateParseTree(contextFreeGrammar, new Word("(((x+(x)*x)*x)*x)"));
        node.print();
        assertNotNull(node);
    }

    @Test
    public void generateParseTree_secondExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode node = parser.generateParseTree(contextFreeGrammar, new Word("x*x+x+x*(x)"));
        node.print();
        assertNotNull(node);
    }

    @Test
    public void generateParseTree_thirdExampleStringUnderTest_isAcceptedByGrammar() {
        IParser parser = new ParserSecondAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode node = parser.generateParseTree(contextFreeGrammar, new Word("(x*x)*x+x+(x)"));
        node.print();
        assertNotNull(node);
    }
}
