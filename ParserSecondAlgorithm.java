import computation.contextfreegrammar.*;
import computation.parser.IParser;
import computation.parsetree.ParseTreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ParserSecondAlgorithm implements the second algorithm.
 */
public class ParserSecondAlgorithm implements IParser {

    /**
     * This method provides an implementation of IParser::isInLanguage interface.
     * @param cfg the context free grammar
     * @param w the word to test
     * @return - a boolean value indicating if the String is in the language
     */
    @Override
    public boolean isInLanguage(ContextFreeGrammar cfg, Word w) {
        Pair symbolAndNode = findSymbolAndNodeForWord(cfg, w);
        return symbolAndNode != null;
    }

    /**
     * This method provides an implementation of IParser::generateParseTree interface.
     * @param cfg the context free grammar
     * @param w the word to test
     * @return - a valid ParseTreeNode for the String or null
     */
    @Override
    public ParseTreeNode generateParseTree(ContextFreeGrammar cfg, Word w) {
        Pair symbolAndNode = findSymbolAndNodeForWord(cfg, w);

        if(symbolAndNode == null){
            return null;
        }

        return symbolAndNode.getNode();
    }

    /**
     * This method is handles the private implementation that is common to both
     * isInLanguage() and generateParseTree(). It returns a Pair of Symbol and ParseTreeNode or null.
     * Throws an IllegalArgumentException if the grammar parameter is not in CNF.
     * @param cfg - a context free grammar in CNF
     * @param w - String being tested
     * @return - Pair of Symbol and ParseTreeNode for the String being tested or null.
     */
    private Pair findSymbolAndNodeForWord(ContextFreeGrammar cfg, Word w){
        if(!cfg.isInChomskyNormalForm()){
            throw new IllegalArgumentException("The grammar provided is not in CNF.");
        }

        //handle the base case with the empty string
        if(w.equals(Word.emptyWord)){
            return findSymbolAndNodeForEmptyString(cfg);
        }

        List<Rule> rulesWithOutEmptyStrings = cfg.getRules().stream()
                                         .filter(rule -> !rule.getExpansion().equals(Word.emptyWord))
                                         .collect(Collectors.toList());

        //Get the first row of the sub-problem table.
        List<List<List<Pair>>> subProblemTable =  getFirstRowOfSubProblemTable(rulesWithOutEmptyStrings, w);

        //Fill the remaining table rows
        subProblemTable = fillRemainingTableRows(rulesWithOutEmptyStrings, w, subProblemTable);

        //Check for start variable to accept or reject string.
        List<Pair> finalPairs = getListAt(subProblemTable, 0, w.length()-1);
        for (Pair pair : finalPairs) {
            if(pair.getSymbol().equals(cfg.getStartVariable())){
                return pair;
            }
        }

        //When we get here we couldn't find a valid case, so return null.
        return null;
    }

    /**
     * Method that takes a grammar and returns the Pair of Symbol and ParseTreeNode that corresponds to the empty String.
     * @param cfg - The context free grammar
     * @return - Pair of Symbol and ParseTreeNode that corresponds to the empty String or null
     */
    private Pair findSymbolAndNodeForEmptyString(ContextFreeGrammar cfg) {
        List<Rule> ruleWithStartVariableThatExpandToTheEmptyString = cfg.getRules().stream()
                                                                                   .filter(rule -> rule.getExpansion().equals(Word.emptyWord) &&
                                                                                           rule.getVariable().equals(cfg.getStartVariable()))
                                                                                   .collect(Collectors.toList());
        //No rule exists that maps the start variable to the empty string.
        //Or if we found more than one such rule return null.
        if(ruleWithStartVariableThatExpandToTheEmptyString.size() != 1){
            return null;
        }

        //We found the rule that meets the condition
        Rule rule = ruleWithStartVariableThatExpandToTheEmptyString.get(0);
        ParseTreeNode node= ParseTreeNode.emptyParseTree(rule.getVariable());
        return new Pair(rule.getVariable(), node);

    }

    /**
     * Creates the first row of the sub-problem table. Note that the first row is diagonal.
     * @param rules - List of rules
     * @param w - String being tested or parsed.
     * @return - Returns a list of list of list of Pairs which represents the sub-problem table with the first row filled out
     */
    private List<List<List<Pair>>> getFirstRowOfSubProblemTable(List<Rule> rules, Word w) {
        List<Rule> rulesWithTerminalExpansion = rules.stream()
                                                     .filter(rule -> rule.getExpansion().isTerminal())
                                                     .collect(Collectors.toList());

        List<List<List<Pair>>> subProblemTable = new ArrayList<>();

        for (int i = 0; i < w.length(); i++) {
            for (Rule rule : rulesWithTerminalExpansion) {
                if(rule.getExpansion().get(0).equals(w.get(i))){

                    List<Pair> pairs = getListAt(subProblemTable, i, i);
                    ParseTreeNode node = new ParseTreeNode(rule.getVariable(), new ParseTreeNode(w.get(i)));
                    Pair pair = new Pair(rule.getVariable(), node);
                    pairs.add(pair);
                }
            }
        }

        return subProblemTable;
    }

    /**
     * Fills the remaining table rows for the sub-problem table.
     *
     * @param rules - List of rules
     * @param w - String being tested or parsed.
     * @param subProblemTable - the sub-problem table with the first row filled out
     * @return - Returns a list of list of list of Pairs which represents the sub-problem table fully filled out
     */
    private List<List<List<Pair>>> fillRemainingTableRows(List<Rule> rules,
                                                          Word w,
                                                          List<List<List<Pair>>> subProblemTable) {

        List<Rule> rulesWithNonTerminalExpansion =rules.stream()
                                                       .filter(rule -> !rule.getExpansion().isTerminal())
                                                       .collect(Collectors.toList());

        for (int l = 2; l <= w.length(); l++) {
            for (int i = 0; i < w.length() - l + 1; i++) {
                int j = i + l - 1;
                for (int k = i; k <= j - 1; k++) {
                    for (Rule rule : rulesWithNonTerminalExpansion) {
                        List<Pair> firstVariables = getListAt(subProblemTable, i, k);
                        List<Pair> secondVariables = getListAt(subProblemTable, k+1, j);
                        for (Pair firstPair : firstVariables) {
                            for (Pair secondPair : secondVariables) {
                                if(rule.getExpansion().equals(new Word(firstPair.getSymbol(), secondPair.getSymbol()))){
                                    List<Pair> pairs = getListAt(subProblemTable, i, j);
                                    ParseTreeNode node = new ParseTreeNode(rule.getVariable(), firstPair.getNode(), secondPair.getNode());
                                    Pair pair = new Pair(rule.getVariable(), node);
                                    pairs.add(pair);
                                }
                            }
                        }
                    }
                }
            }

        }

        return subProblemTable;
    }

    /**
     * getListAt() provides a safe way to get an entry from the sub-problem table.
     * It ensure that array list grows to the size of the requested cell and returns the requested cell.
     * @param subProblemTable - the sub-problem table
     * @param i - the table cell row index
     * @param j - the table cell column index
     * @return - the contents of the request cell as a list of Pair
     */
    private List<Pair> getListAt(List<List<List<Pair>>> subProblemTable, int i, int j) {
        if(subProblemTable.size() < i+1){
            subProblemTable.add(i, new ArrayList<>());
        }

        if(subProblemTable.get(i).size() < j+1){
            for (int k = subProblemTable.get(i).size(); k <= j; k++) {
                subProblemTable.get(i).add(k,new ArrayList<>());
            }
        }

        return subProblemTable.get(i).get(j);

    }

    /**
     * Convenience private data structure for storing a Symbol and its ParseTreeNode together.
     */
    private class Pair {
        private Symbol symbol;
        private ParseTreeNode node;

        public Pair(Symbol symbol, ParseTreeNode node) {
            this.symbol = symbol;
            this.node = node;
        }

        public Symbol getSymbol() {
            return symbol;
        }

        public ParseTreeNode getNode() {
            return node;
        }
    }
}
