import computation.contextfreegrammar.ContextFreeGrammar;
import computation.contextfreegrammar.Rule;
import computation.contextfreegrammar.Variable;
import computation.contextfreegrammar.Word;
import computation.parser.IParser;
import computation.parsetree.ParseTreeNode;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Create a new Java class that has the same name as the class under test
 * with "Test" appended to the end of the class name.
 * In this example, the class under Test is ParserFirstAlgorithm,
 * so my test class is ParserFirstAlgorithmTest
 *
 * If you are using IntelliJ, the keyboard shortcut is control+shift+t on linux and
 * and windows and command+shift+t on Mac.
 *
 * Note that using the keyboard shortcut also gives you options to include a version of
 * JUnit in your project. I am currently using JUnit4
 */
public class ParserFirstAlgorithmTest {

    @Test(expected = IllegalArgumentException.class)
    public void isInLanguage(){
        ContextFreeGrammar contextFreeGrammar = new ContextFreeGrammar(List.of(new Rule(new Variable('X'), new Word("XYZ"))));
        Word wordUnderTest = new Word("x+x");
        IParser parser = new ParserFirstAlgorithm();
        parser.isInLanguage(contextFreeGrammar, wordUnderTest);


    }



    /**
     * The general idea with unit testing is to create public method with the
     * @Test annotation. In IntelliJ or eclipse, the runs icon shows up next the
     * method header that enables you to run the code within the test method.
     */
    @Test
    public void isInLanguage_xPlusxUnderTest_isAcceptedByGrammar() {
        //Instantiate your class under test.
        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();

        //Create any objects that you test class or method under test require as inputs
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        Word wordUnderTest = new Word("x+x");

        //Call you method under and store results in a variable
        boolean inLanguage = parserFirstAlgorithm.isInLanguage(contextFreeGrammar, wordUnderTest);

        //Use on of the assert method appropriate for your context.
        assertTrue(inLanguage);

        //Note that most of these steps can be inlined to make your code more concise.
    }


    @Test
    public void isInLanguage_xTimesxUnderTest_isAcceptedByGrammar() {
        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parserFirstAlgorithm.isInLanguage(contextFreeGrammar, new Word("x*x"));

        assertTrue(inLanguage);
    }


//    @Test //todo this runs out of memory
//    public void isInLanguage_longerStringUnderTest_isAcceptedByGrammar() {
//        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
//        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
//        boolean inLanguage = parserFirstAlgorithm.isInLanguage(contextFreeGrammar, new Word("(((x+(x)*x)*x)*x"));
//
//        assertTrue(inLanguage);
//    }

    @Test
    public void isInLanguage_xxUnderTest_isRejectedByGrammar() {
        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parserFirstAlgorithm.isInLanguage(contextFreeGrammar, new Word("xx"));

        assertFalse(inLanguage);
    }

    @Test
    public void isInLanguage_bracketsUnderTest_isRejectedByGrammar() {
        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        boolean inLanguage = parserFirstAlgorithm.isInLanguage(contextFreeGrammar, new Word("()"));

        assertFalse(inLanguage);
    }

    @Test
    public void generateParseTree_xUnderTest_expectParseTreeNotNull() {

        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode parseTreeNode = parserFirstAlgorithm.generateParseTree(contextFreeGrammar, new Word("x"));

        parseTreeNode.print();

        assertNotNull(parseTreeNode);

    }

    @Test
    public void generateParseTree_xTimesxUnderTest_expectParseTreeNotNull() {

        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode parseTreeNode = parserFirstAlgorithm.generateParseTree(contextFreeGrammar, new Word("x*x"));

        parseTreeNode.print();

        assertNotNull(parseTreeNode);

    }

    @Test
    public void generateParseTree_xTimesx_plusx_underTest_expectParseTreeNotNull() {

        IParser parserFirstAlgorithm = new ParserFirstAlgorithm();
        ContextFreeGrammar contextFreeGrammar = MyGrammar.makeGrammar();
        ParseTreeNode parseTreeNode = parserFirstAlgorithm.generateParseTree(contextFreeGrammar, new Word("x*x+x"));

        if(parseTreeNode != null){
            parseTreeNode.print();
        }

        assertNotNull(parseTreeNode);

    }
}
